#!/bin/sh

## Sourceable file with common variables for other scripts related to docker images

export docker_images='runtime-dependencies runtime-prebuild-dependencies runtime-build-dependencies runtime-build-test-dependencies runtime-build-e2etest-dependencies'

export docker_architectures='amd64 arm64'
